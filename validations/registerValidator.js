const validator = require('validator');
const isEmpty = require('../utils/isEmpty');

module.exports = (data) => {
  const { username, email, password } = data;
  const error = {};
  if (!validator.isEmail(email)) {
    error.email = 'Invalid Email';
  }

  if (!validator.isLength(username, { min: 4, max: 30 })) {
    error.username = 'Username must be between 4 to 30 characters long';
  }

  if (isEmpty(username)) {
    error.username = 'Username is required';
  }

  if (!validator.isLength(password, { min: 6, max: 30 })) {
    error.password = 'Password must be between 6 to 30 characters long';
  }
  if (isEmpty(password)) {
    error.password = 'Password is required';
  }

  return { error, isValid: isEmpty(error) };
};
