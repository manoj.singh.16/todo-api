const express = require('express');
const { getAllSubTasks, postSubTask, getSingleSubTask, updateSubTask, deleteSubTask } = require('../controllers/subTasksController');
const verifyAuth = require('../middlewares/verifyAuth');

const router = express.Router();
// Get all subtasks
router.get('/', getAllSubTasks);

// Get single subtask
router.get('/:id', getSingleSubTask);

// Post subtask
router.post('/', verifyAuth, postSubTask);

// update subtask
router.patch('/:id', verifyAuth, updateSubTask);

// delete subtask
router.delete('/:id', verifyAuth, deleteSubTask);

module.exports = router;
