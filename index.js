const express = require('express');
require('dotenv').config();
const config = require('./config');
const logger = require('./middlewares/logger');
const authRoute = require('./routes/authRoutes');
const taskRoute = require('./routes/taskRoutes');
const subTaskRoutes = require('./routes/subTaskRoutes');

const app = express();

app.use(logger);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => res.json({ msg: 'Success' }));

app.use('/auth', authRoute);

app.use('/tasks', taskRoute);

app.use('/sub-tasks', subTaskRoutes);

app.use('/', (req, res) => res.status(404).json({ msg: 'Not found' }));

app.listen(config.PORT, () => console.log(`Server is listening on port ${config.PORT}`));
