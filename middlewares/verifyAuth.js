const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try {
    const authHeaders = req.headers['authorization'];
    const token = authHeaders.split(' ')[1];

    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    req.user = decoded;
    next();
  } catch (error) {
    res.status(403).json({ msg: 'not authorized' });
  }
};
