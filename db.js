const { Pool } = require('pg');

const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.HOST,
  database: process.env.DATABASE,
  password: process.env.PASSWORD,
  port: process.env.PORT,
});

pool
  .connect()
  .then(() => console.log(`Database connected`))
  .catch((error) => console.log(error));

module.exports = pool;
