const generateToken = require('../utils/generateToken');
const db = require('../db');
const config = require('../config.json');

module.exports = (user) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { username, id, password } = user;
      const accressTokenPromise = generateToken({ username, id }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: config.accessTimeout });
      const refreshTokenPromise = generateToken({ username, id }, process.env.REFRESH_TOKEN_SECRET + password);

      const [accessToken, refreshToken] = await Promise.all([accressTokenPromise, refreshTokenPromise]);

      const tokenQuery = await db.query('select * from refresh_tokens where user_id = $1', [id]);
      const { rows: token_res } = tokenQuery;
      if (token_res.length > 0) {
        db.query('update refresh_tokens set token = $1 where user_id = $2', [refreshToken, id]);
      } else {
        db.query('insert into refresh_tokens (user_id,token) values ($1,$2)', [id, refreshToken]);
      }

      resolve({ accessToken, refreshToken });
    } catch (error) {
      console.log(error);
    }
  });
};
