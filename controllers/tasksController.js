const db = require('../db');

const getAllTasks = async (req, res) => {
  try {
    const { rows } = await db.query('select * from tasks');

    res.json({ data: rows });
  } catch (error) {
    console.log(error);
  }
};

const getSingleTask = async (req, res) => {
  try {
    const taskId = req.params.id;
    const { rows } = await db.query('select * from tasks where id = $1', [taskId]);

    if (rows[0]) {
      res.json({ data: rows[0] });
    } else {
      res.json({ data: {} });
    }
  } catch (error) {
    console.log(error);
  }
};

const postTask = async (req, res) => {
  try {
    const { user } = req;
    const { text } = req.body;

    const { rows } = await db.query('insert into tasks (text,user_id) values ($1,$2) returning *', [text, user.id]);

    res.status(201).json({ data: rows[0] });
  } catch (error) {
    console.log(error);
  }
};

const deleteTask = async (req, res) => {
  try {
    const taskId = req.params.id;

    const { user } = req;
    const { rows } = await db.query('select * from tasks where id = $1', [taskId]);

    if (rows[0].user_id !== user.id) {
      return res.status(401).json({ error: 'Not authorized' });
    }
    const { rowCount } = await db.query('delete from tasks where id = $1', [taskId]);

    if (rowCount > 0) {
      res.json({
        data: {
          msg: 'deleted successfully',
        },
      });
    } else {
      res.status(400).json({
        data: {
          msg: 'no task with the given id',
        },
      });
    }
  } catch (error) {
    res.status(400).json({ error: 'Invalid request' });
  }
};

const updateTask = async (req, res) => {
  try {
    const taskId = req.params.id;
    const { text, is_completed } = req.body;

    const { user } = req;
    const { rows } = await db.query('select * from tasks where id = $1', [taskId]);

    if (rows[0].user_id !== user.id) {
      return res.status(401).json({ error: 'Not authorized' });
    }

    if (text && is_completed) {
      const { rows } = await db.query('update tasks set text = $1, is_completed = $2 where id = $3 returning *', [text, is_completed, taskId]);
      return res.json({ data: rows[0] });
    } else if (text) {
      const { rows } = await db.query('update tasks set text = $1 where id = $2 returning *', [text, taskId]);
      return res.json({ data: rows[0] });
    } else if (is_completed) {
      const { rows } = await db.query('update tasks set is_completed = $1 where id = $2 returning *', [is_completed, taskId]);
      res.json({ data: rows[0] });
    } else {
      res.status(400).json({ error: 'Invalid request' });
    }
  } catch (error) {
    res.status(400).json({ error: 'Invalid request' });
  }
};

module.exports = {
  getAllTasks,
  postTask,
  getSingleTask,
  deleteTask,
  updateTask,
};
