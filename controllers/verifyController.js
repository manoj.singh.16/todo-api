const jwt = require('jsonwebtoken');

module.exports = (req, res) => {
  try {
    const accessTokenHeader = req.headers['authorization'];
    const accessToken = accessTokenHeader.split(' ')[1];
    const decoded = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);

    res.json({ username: decoded.username, id: decoded.id });
  } catch (error) {
    res.status(400).json({ msg: 'Invalid token' });
  }
};
