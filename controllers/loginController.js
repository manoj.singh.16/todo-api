const loginValidator = require('../validations/loginValidator');
const bcrypt = require('bcrypt');
const db = require('../db');
const loginUser = require('../modules/loginUser');

module.exports = async (req, res) => {
  const { error, isValid } = loginValidator(req.body);

  if (!isValid) {
    return res.status(400).json(error);
  }

  try {
    const { username, password } = req.body;
    const { rows } = await db.query('select * from users where username = $1', [username]);

    if (rows.length <= 0) {
      return res.status(401).json({ msg: 'No user found' });
    }

    const user = rows[0];
    const result = await bcrypt.compare(password, user.password);

    if (!result) {
      return res.status(401).json({ msg: 'Password incorrect' });
    }

    const { accessToken, refreshToken } = await loginUser(user);

    res.json({ accessToken, refreshToken });
  } catch (error) {
    console.log(error);
  }
};
