const db = require('../db');

module.exports = async (req, res) => {
  try {
    const { user } = req;
    const { rowCount } = await db.query('delete from users where id = $1', [user.id]);
    if (rowCount > 0) {
      res.json({ msg: 'Successfully deactivated' });
    } else {
      res.status(400).json({ msg: 'User doesnt exist' });
    }
  } catch (error) {
    console.log(error);
  }
};
