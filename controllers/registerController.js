const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const registerValidator = require('../validations/registerValidator');
const db = require('../db');
const { saltRounds } = require('../config.json');
const loginUser = require('../modules/loginUser');

module.exports = async (req, res) => {
  const { error, isValid } = registerValidator(req.body);

  if (!isValid) {
    return res.status(400).json(error);
  }

  try {
    const { username, password, email } = req.body;

    const data = await db.query('select username,email from users where username = $1 or email = $2', [username, email]);

    if (data.rows[0] && username === data.rows[0].username) {
      return res.status(400).json({ msg: `username already exist` });
    } else if (data.rows[0] && email === data.rows[0].email) {
      return res.status(400).json({ msg: `email already exist` });
    }

    const hash = await bcrypt.hash(password, saltRounds);

    const { rows } = await db.query('insert into users (username,password,email) values ($1,$2,$3) returning *', [username, hash, email]);
    const user = rows[0];
    const { accessToken, refreshToken } = await loginUser(user);

    res.status(201).json({ accessToken, refreshToken });
  } catch (error) {
    console.log(error);
  }
};
