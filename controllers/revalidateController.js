const jwt = require('jsonwebtoken');
const generateToken = require('../utils/generateToken');
const db = require('../db');

module.exports = async (req, res) => {
  try {
    const refreshHeader = req.headers['x-refresh-token'];
    const refreshToken = refreshHeader.split(' ')[1];
    const { username, id } = jwt.decode(refreshToken);

    const { rows: refreshQuery } = await db.query('select * from refresh_tokens where user_id = $1', [id]);
    const refreshData = refreshQuery[0];

    if (refreshData.token === refreshToken) {
      const { rows } = await db.query('select * from users where id = $1', [id]);
      const user = rows[0];

      jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET + user.password, async (error, decoded) => {
        if (error) {
          console.log(error);
        } else {
          const accessToken = await generateToken({ username, id }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: 3600 });
          res.json({ accessToken });
        }
      });
    } else {
      throw new Error('Invalid refresh token');
    }
  } catch (error) {
    res.status(400).json({ msg: 'Invalid token' });
  }
};
