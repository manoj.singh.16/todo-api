const jwt = require('jsonwebtoken');
const db = require('../db');

module.exports = async (req, res) => {
  try {
    const refreshHeader = req.headers['x-refresh-token'];
    const refreshToken = refreshHeader.split(' ')[1];
    const { username, id } = jwt.decode(refreshToken);

    const { rowCount } = await db.query('delete from refresh_tokens where token = $1', [refreshToken]);
    if (rowCount > 0) {
      res.json({ msg: 'Logout Success' });
    } else {
      throw new Error('No Refresh Token in DB');
    }
  } catch (error) {
    res.status(401).json({ msg: 'Invalid Token' });
  }
};
