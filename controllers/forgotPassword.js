const db = require('../db');
const generateToken = require('../utils/generateToken');

module.exports = async (req, res) => {
  try {
    const { email } = req.body;

    const { rows } = await db.query('select * from users where email = $1', [email]);
    const user = rows[0];

    const token = await generateToken({ username: user.username, id: user.id, email: user.email }, process.env.PASSWORD_TOKEN_SECRET + user.password);

    res.json({ token });
  } catch (error) {
    console.log(error);
    res.status(404).json({ msg: 'No user found with that email' });
  }
};
