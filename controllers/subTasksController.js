const db = require('../db');

const getAllSubTasks = async (req, res) => {
  try {
    const { rows } = await db.query('select * from sub_tasks');

    res.json({ data: rows });
  } catch (error) {
    console.log(error);
  }
};

const getSingleSubTask = async (req, res) => {
  try {
    const subTaskId = req.params.id;
    const { rows } = await db.query('select * from sub_tasks where id = $1', [subTaskId]);

    if (rows[0]) {
      res.json({ data: rows[0] });
    } else {
      res.json({ data: {} });
    }
  } catch (error) {
    console.log(error);
  }
};

const postSubTask = async (req, res) => {
  try {
    const { text, task_id } = req.body;

    const { rows } = await db.query('insert into sub_tasks (text,task_id) values ($1,$2) returning *', [text, task_id]);

    res.status(201).json({ data: rows[0] });
  } catch (error) {
    console.log(error);
  }
};

const deleteSubTask = async (req, res) => {
  try {
    const subTaskId = req.params.id;

    const { user } = req;
    const { rows } = await db.query('select * from tasks join sub_tasks on tasks.id = sub_tasks.task_id where sub_tasks.id = $1', [subTaskId]);

    if (rows.length > 0 && rows[0].user_id !== user.id) {
      return res.status(401).json({ error: 'Not authorized' });
    } else {
      const { rowCount } = await db.query('delete from sub_tasks where id = $1', [subTaskId]);

      if (rowCount > 0) {
        res.json({
          data: {
            msg: 'deleted successfully',
          },
        });
      } else {
        res.status(400).json({
          data: {
            msg: 'no task with the given id',
          },
        });
      }
    }
  } catch (error) {
    res.status(400).json({ error: 'Invalid request' });
  }
};

const updateSubTask = async (req, res) => {
  try {
    const subTaskId = req.params.id;
    const { text, is_completed } = req.body;

    const { user } = req;
    const { rows } = await db.query('select * from tasks join sub_tasks on tasks.id = sub_tasks.task_id where sub_tasks.id = $1', [subTaskId]);

    if (rows[0].user_id !== user.id) {
      return res.status(401).json({ error: 'Not authorized' });
    }

    if (text && is_completed) {
      const { rows } = await db.query('update sub_tasks set text = $1, is_completed = $2 where id = $3 returning *', [text, is_completed, subTaskId]);
      res.json({ data: rows[0] });
    } else if (text) {
      const { rows } = await db.query('update sub_tasks set text = $1 where id = $2 returning *', [text, subTaskId]);
      res.json({ data: rows[0] });
    } else if (is_completed) {
      const { rows } = await db.query('update sub_tasks set is_completed = $1 where id = $2 returning *', [is_completed, subTaskId]);
      res.json({ data: rows[0] });
    } else {
      res.status(400).json({ error: 'Invalid request' });
    }
  } catch (error) {
    res.status(400).json({ error: 'Invalid request' });
  }
};

module.exports = {
  getAllSubTasks,
  postSubTask,
  getSingleSubTask,
  updateSubTask,
  deleteSubTask,
};
