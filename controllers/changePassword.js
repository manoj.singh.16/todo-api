const jwt = require('jsonwebtoken');
const db = require('../db');
const bcrypt = require('bcrypt');
const config = require('../config.json');
const changePasswordValidator = require('../validations/changePasswordValidator');

module.exports = async (req, res) => {
  try {
    const tokenHeader = req.headers['x-forgot-token'];
    const forgotToken = tokenHeader.split(' ')[1];
    const { username, id, email } = jwt.decode(forgotToken);

    const { rows } = await db.query('select * from users where id = $1', [id]);
    const user = rows[0];

    const decoded = jwt.verify(forgotToken, process.env.PASSWORD_TOKEN_SECRET + user.password);

    const { error, isValid } = changePasswordValidator(req.body);

    if (!isValid) {
      return res.status(400).json(error);
    }

    const { password: newPassword } = req.body;

    const newHash = await bcrypt.hash(newPassword, config.saltRounds);
    const passUpdateQuery = await db.query('update users set password = $1 where id = $2', [newHash, id]);

    res.json({ msg: 'password updated' });
  } catch (error) {
    res.status(400).json({ msg: 'Invalid Request' });
  }
};
