CREATE database todo_database;

CREATE table users (
    id serial primary key,
    username varchar(30) not null,
    password varchar(200) not null,
    email varchar(50) not null
);

CREATE table refresh_tokens (
    id serial primary key,
    user_id int not null,
    token varchar(200) 
);

CREATE table tasks (
    id serial primary key,
    text varchar(200) not null,
    is_completed boolean default false,
    user_id int,
    constraint fk_user_id foreign key(user_id) references users(id) on delete cascade
);

CREATE table sub_tasks (
    id serial primary key,
    text varchar(200) not null,
    is_completed boolean default false,
    task_id int,
    constraint fk_task_id foreign key(task_id) references tasks(id) on delete cascade
);